# GST.TheCoffee
[Checklist]
[ ] Check-build
[ ] Self-Test
[ ] Self-Review

**[Rules]**

1.	Tạo MR trước khi implement task 
2.	Assign review: 1 người làm 2 người review. người nào review xong sẽ tự xóa tên mình ra khỏi assign
    người còn lại có trách nhiệm merge vào nhánh develop.
3.	Đặt tên nhánh theo format: [tên module]. [tên nhánh làm]
4.	Đặt message commit theo rule sau:

	- [imp].[message commit] : Cho trường hợp implement task.
	- [fix].[message commit] : Cho trường hợp fix bugs hoặc fix comment.
	- [add].[message commit] : Cho trường hợp add resources.


5.	Đặt title MR rule sau:
- [Tên Module]. [imp].[title] : Cho trường hợp implement task.
- [Tên Module]. [fix].[title] : Cho trường hợp fix bugs hoặc fix comment.
- [Tên Module]. [add].[title] : Cho trường hợp add resources.

7.	Phải apply check list cho tất cả các MR.

**[Solution]**

- 

**[Known Issue]**

- N/A

**[Evidences]** (*image or video*...)

-