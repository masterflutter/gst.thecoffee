import 'package:GST.TheCoffee/phone/phone.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';

class BuildHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Device.get().isPhone ? Phone() : Container();
  }
}
