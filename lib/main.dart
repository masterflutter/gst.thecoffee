import 'package:GST.TheCoffee/bloc/screen_detail/theme/theme_bloc.dart';
import 'package:GST.TheCoffee/bloc/screen_detail/theme/theme_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:path_provider/path_provider.dart';

import 'dependencies.dart';
import 'home.dart';
import 'utility/app_configuration.dart';
import 'utility/global_translation.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final rootFolder = await getApplicationDocumentsDirectory();
  final injector = AppDependencies.initialise(rootFolder.path);
  await injector.get<GlobalTranslations>().init();
  await injector.get<AppConfiguration>().init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  GlobalTranslations translator =
      AppDependencies.injector.get<GlobalTranslations>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ThemeBloc(),
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: (context, state){
          if(state is LoadedThemeState){
            return  MaterialApp(
              color: Theme.of(context).backgroundColor,
              title: 'Flutter Demo',
              theme: state.themeData,
              localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              debugShowCheckedModeBanner: false,
              supportedLocales: translator.supportedLocales(),
              initialRoute: getInitialRoute(),
              routes: buildRoutes(),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      ),
    );
  }

  String getInitialRoute() {
    return '/';
  }

  Map<String, WidgetBuilder> buildRoutes() {
    return {
      '/': (context) => BuildHomePage(),
    };
  }
}
