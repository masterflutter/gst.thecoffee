import 'package:GST.TheCoffee/utility/app_configuration.dart';
import 'package:GST.TheCoffee/utility/file_utility.dart';
import 'package:GST.TheCoffee/utility/global_translation.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';

// ignore: avoid_classes_with_only_static_members
class AppDependencies {
  static Injector get injector => Injector.getInjector();

  static Injector initialise(String rootFolder) {
    injector.map<GlobalTranslations>((injector) => GlobalTranslations(), isSingleton: true);
    injector.map<AppConfiguration>((injector) => AppConfiguration(), isSingleton: true);
    injector.map<FileUtility>((injector) => FileUtility(rootFolder), isSingleton: true);
    return injector;
  }
}
