import 'package:GST.TheCoffee/model/store/store.dart';
import 'package:json_annotation/json_annotation.dart';

part 'district.g.dart';

@JsonSerializable()
class District {
  District({
    this.id,
    this.name,
    this.count,
    this.stateName,
    this.stores,
  });

  factory District.fromJson(Map<String, dynamic> json) =>  _$DistrictFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictToJson(this);

  final String id;
  final String name;
  final String count;
  final String stateName;
  final List<Store> stores;
}
