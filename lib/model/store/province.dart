import 'package:GST.TheCoffee/model/store/district.dart';
import 'package:json_annotation/json_annotation.dart';

part 'province.g.dart';

@JsonSerializable()
class Province {
  Province({
    this.id,
    this.name,
    this.count,
    this.districts,
  });

  factory Province.fromJson(Map<String, dynamic> json) => _$ProvinceFromJson(json);

  Map<String, dynamic> toJson() => _$ProvinceToJson(this);

  final String id;
  final String name;
  final String count;
  final List<District> districts;
}
