import 'package:GST.TheCoffee/model/store/address.dart';
import 'package:json_annotation/json_annotation.dart';

part 'store.g.dart';

@JsonSerializable()
class Store {
  Store({
    this.id,
    this.name,
    this.distance,
    this.address,
    this.phone,
    this.openingTime,
    this.closingTime,
    this.status,
    this.images,
    this.latitude,
    this.longitude,
  });

  factory Store.fromJson(Map<String, dynamic> json) => _$StoreFromJson(json);

  Map<String, dynamic> toJson() => _$StoreToJson(this);

  final String id;
  final String name;
  final String distance;
  final Address address;
  final String phone;
  final String openingTime;
  final String closingTime;
  final String status;
  final List<String> images;
  final double latitude;
  final double longitude;
}