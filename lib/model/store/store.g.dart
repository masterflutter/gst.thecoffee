// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Store _$StoreFromJson(Map<String, dynamic> json) {
  return Store(
    id: json['id'].toString() as String,
    name: json['name'] as String,
    distance: json['distance'] as String,
    address: json['address'] == null
        ? null
        : Address.fromJson(json['address'] as Map<String, dynamic>),
    phone: json['phone'] as String,
    openingTime: json['opening_time'] as String,
    closingTime: json['closing_time'] as String,
    status: json['status'] as String,
    images: (json['images'] as List)?.map((e) => e as String)?.toList(),
    latitude: double.parse(json['latitude']),
    longitude: double.parse(json['longitude']),
  );
}

Map<String, dynamic> _$StoreToJson(Store instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'distance': instance.distance,
      'address': instance.address,
      'phone': instance.phone,
      'openingTime': instance.openingTime,
      'closingTime': instance.closingTime,
      'status': instance.status,
      'images': instance.images,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
