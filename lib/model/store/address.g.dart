// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) {
  return Address(
    street: json['street'] as String,
    ward: json['ward'] as String,
    district: json['district'] as String,
    state: json['state'] as String,
    country: json['country'] as String,
    fullAddress: json['full_address'] as String,
  );
}

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'street': instance.street,
      'ward': instance.ward,
      'district': instance.district,
      'state': instance.state,
      'country': instance.country,
      'fullAddress': instance.fullAddress,
    };
