import 'package:GST.TheCoffee/model/store/province.dart';
import 'package:json_annotation/json_annotation.dart';

part 'province_list.g.dart';

@JsonSerializable()
class ProvinceList {
  ProvinceList({
    this.states,
  });

  factory ProvinceList.fromJson(Map<String, dynamic> json) => _$ProvinceListFromJson(json);

  Map<String, dynamic> toJson() => _$ProvinceListToJson(this);

  final List<Province> states;
}
