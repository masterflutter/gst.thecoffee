// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'province.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Province _$ProvinceFromJson(Map<String, dynamic> json) {
  return Province(
    id: json['id'].toString() as String,
    name: json['name'] as String,
    count: json['count'].toString() as String,
    districts: (json['districts'] as List)
        ?.map((e) =>
            e == null ? null : District.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ProvinceToJson(Province instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'count': instance.count,
      'districts': instance.districts,
    };
