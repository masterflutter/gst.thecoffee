import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable()
class Address {
  Address({
    this.street,
    this.ward,
    this.district,
    this.state,
    this.country,
    this.fullAddress,
  });

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);

  final String street;
  final String ward;
  final String district;
  final String state;
  final String country;
  final String fullAddress;
}