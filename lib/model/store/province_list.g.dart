// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'province_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProvinceList _$ProvinceListFromJson(Map<String, dynamic> json) {
  return ProvinceList(
    states: (json['states'] as List)
        ?.map((e) =>
            e == null ? null : Province.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ProvinceListToJson(ProvinceList instance) =>
    <String, dynamic>{
      'states': instance.states,
    };
