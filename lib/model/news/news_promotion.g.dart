// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_promotion.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsPromotion _$NewsPromotionFromJson(Map<String, dynamic> json) {
  return NewsPromotion(
    json['id'] as String,
    json['title'] as String,
    json['url'] as String,
    json['image'] as String,
    json['content'] as String,
    json['date'] == null ? null : DateTime.parse(json['date'] as String),
    json['button'] as String,
    json['deepLink'] as String,
    json['effects'] as String,
    json['shareUrl'] as String,
  );
}

Map<String, dynamic> _$NewsPromotionToJson(NewsPromotion instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'url': instance.url,
      'image': instance.image,
      'content': instance.content,
      'date': instance.date?.toIso8601String(),
      'button': instance.button,
      'deepLink': instance.deepLink,
      'effects': instance.effects,
      'shareUrl': instance.shareUrl,
    };
