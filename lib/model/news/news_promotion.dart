import 'package:json_annotation/json_annotation.dart';

part 'news_promotion.g.dart';

@JsonSerializable()
class NewsPromotion {
  NewsPromotion(
      this.id,
      this.title,
      this.url,
      this.image,
      this.content,
      this.date,
      this.button,
      this.deepLink,
      this.effects,
      this.shareUrl);

  factory NewsPromotion.fromJson(Map<String, dynamic> json) => _$NewsPromotionFromJson(json);

  Map<String, dynamic> toJson() => _$NewsPromotionToJson(this);

  final String id;
  final String title;
  final String url;
  final String image;
  final String content;
  final DateTime date;
  final String button;
  final String deepLink;
  final String effects;
  final String shareUrl;
}