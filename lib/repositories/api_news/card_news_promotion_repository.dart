import 'dart:convert';

import 'package:GST.TheCoffee/model/news/news_promotion.dart';
import 'package:http/http.dart' as http;

const String url = 'https://api.thecoffeehouse.com/api/v2/news_promotion';

class CardNewPromotionRepository{

  Future<List<NewsPromotion>> getAllCard() async {
    final response = await http.Client().get(url);

    if(response.statusCode != 200){
      throw Exception('error get info from card news');
    }
    return allCardNews(response.body);
  }

  List<NewsPromotion> allCardNews(String str){
    final jsonData = jsonDecode(str);
    return List<NewsPromotion>.from(jsonData.map( (x) => NewsPromotion.fromJson(x)));
  }
}