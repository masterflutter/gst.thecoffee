import 'dart:convert';
import 'package:GST.TheCoffee/model/store/province_list.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

class StoresAPIClient {
  StoresAPIClient({@required this.httpClient}) : assert(httpClient != null, "");

  static const baseUrl = 'https://api.thecoffeehouse.com';
  final http.Client httpClient;

  Future<ProvinceList> fetchStoresData() async {
    const url = '$baseUrl/api/get_list_store';
    final response = await httpClient.get(url);

    if (response.statusCode != 200) {
      throw Exception("Failed to get all stores");
    }

    final storeJson = jsonDecode(response.body);
    return ProvinceList.fromJson(storeJson);
  }
}