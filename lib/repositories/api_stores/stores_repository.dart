import 'package:GST.TheCoffee/model/store/province_list.dart';
import 'package:GST.TheCoffee/repositories/api_stores/stores_api_client.dart';
import 'package:meta/meta.dart';

class StoresRepository {
  StoresRepository({@required this.storesAPIClient}) : assert(storesAPIClient != null,"");

  final StoresAPIClient storesAPIClient;

  Future<ProvinceList> getStores() async {
    final stores = await storesAPIClient.fetchStoresData();
    return stores;
  }
}
