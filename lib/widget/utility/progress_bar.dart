import 'package:GST.TheCoffee/styles/dimens.dart';
import 'package:flutter/material.dart';

class ProgressBar extends StatelessWidget {
  final int maxCheck = 100; // %
  final double progress;
  final Color color;
  final Color backgroundColor;
  final BorderRadiusGeometry borderRadius;
  final BorderRadiusGeometry _defaultBorderRadius =
      BorderRadius.circular(Dimens.progressBarBorderRadius);

  ProgressBar({
    this.progress,
    this.color,
    this.backgroundColor = Colors.black12,
    this.borderRadius,
  }) : assert(progress == null || (progress >= 0 && progress <= 1));

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: borderRadius ?? _defaultBorderRadius,
          ),
        ),
        Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: ((progress ?? 0) * maxCheck).round(),
                child: Container(
                  decoration: BoxDecoration(
                    color: color ?? Colors.blue,
                    borderRadius: borderRadius ?? _defaultBorderRadius,
                  ),
                ),
              ),
              Expanded(
                flex: maxCheck - ((progress ?? 0) * maxCheck).round(),
                child: Container(),
              ),
            ],
          ),
        )
      ],
    );
  }
}
