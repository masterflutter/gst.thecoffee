import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
  
class MessageBarUtilities {

  @protected
  static const int Info = 0;

  @protected
  static const int Warning = 1;

  @protected
  static const int Error = 2;

  static void showError(BuildContext context, String message, { String title = "" }) {
    buildMessage(Error, message, title: title)..show(context);
  }

  static void showWarning(BuildContext context, String message, { String title = "" }) {
    buildMessage(Warning, message, title: title)..show(context);
  }

  static void showInfo(BuildContext context, String message, { String title = "" }) {
    buildMessage(Info, message, title: title)..show(context);
  }

  @protected
  static Flushbar buildMessage(int messageType, String message, { String title = "" }) {
    var iconData = messageType == Error ? Icons.error : (messageType == Warning ? Icons.warning : Icons.info);
    var leftColor = messageType == Error ? Colors.red[600] : (messageType == Warning ? Colors.yellow[600] : Colors.blue[600]);

    return Flushbar(
      message: message,
      title: title == "" ? null : title,
      icon: Icon(
        iconData,
        size: 28.0,
        color: leftColor,
      ),
      duration: Duration(seconds: 3),
      //leftBarIndicatorColor: leftColor,
      flushbarPosition: FlushbarPosition.TOP,
      margin: EdgeInsets.all(8),
      borderRadius: 8
    );
  }

}