import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingDialog extends StatelessWidget {
  static bool isOpen = false;

  static Future show(BuildContext context, Function method) async {
    open(context);
    var result = await method();
    close(context);
    return result;
  }

  static void open(BuildContext context) {
    if (isOpen) {
      return;
    }

    isOpen = true;
    showDialog(
      context: context,
      builder: (BuildContext context) => LoadingDialog()
    );
  }

  static void close(BuildContext context) {
    if (!isOpen) {
      return;
    }
    Navigator.pop(context);
    isOpen = false;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(    
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Center(
        child: SpinKitCircle(color: Theme.of(context).accentColor, size: 50),
      ));
  }
}