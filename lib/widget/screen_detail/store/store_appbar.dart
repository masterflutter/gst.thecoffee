import 'package:GST.TheCoffee/styles/dimens.dart';
import 'package:GST.TheCoffee/widget/base/base_widget.dart';
import 'package:flutter/material.dart';

class StoreAppbar extends BaseStatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: Dimens.size23),
      height: Dimens.storeAppbarHeight,
      decoration: BoxDecoration(
        border: Border( bottom: BorderSide(width: Dimens.width05)),
        color: Theme.of(context).cardTheme.color,
      ),
      child: Center(
        child: Text(
          translator.text('store_title'),
          style: Theme.of(context).textTheme.title,
        ),
      ),
    );
  }
}