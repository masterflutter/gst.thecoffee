import 'dart:math';

import 'package:GST.TheCoffee/bloc/store/stores_list/stores_bloc.dart';
import 'package:GST.TheCoffee/bloc/store/stores_list/stores_event.dart';
import 'package:GST.TheCoffee/bloc/store/stores_list/stores_state.dart';
import 'package:GST.TheCoffee/model/store/province.dart';
import 'package:GST.TheCoffee/styles/dimens.dart';
import 'package:GST.TheCoffee/widget/base/base_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StoreDropDownBar extends StatefulWidget {
  @override
  _StoreDropDownBarState createState() => _StoreDropDownBarState();
}

class _StoreDropDownBarState extends BaseState<StoreDropDownBar> {
  String _dropdownLabel;
  bool _isTapped = false;

  @override
  void initState() {
    BlocProvider.of<StoresBloc>(context).add(FetchStores());
    _dropdownLabel = translator.text("store_dropdown_initial_label");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _dropdownTapped,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildDropdownBar(_dropdownLabel),
          if (_isTapped)
            BlocBuilder<StoresBloc, StoresState>(
              builder: (context, state) {
                if (state is StoresEmpty) {
                  return Container();
                }
                if (state is StoresLoading) {
                  return Container(
                    margin: const EdgeInsets.only(top: Dimens.size10),
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }

                if (state is StoresLoaded) {
                  final storesList = state.storesList;
                  return _buildDropdownBox(storesList.states);
                }
                return const CircularProgressIndicator();
              },
            ),
        ],
      ),
    );
  }

  void _dropdownTapped() {
    setState(
      () {
        _isTapped = !_isTapped;
      },
    );
  }

  void _storeSelected(String text) {
    setState(() {
      _dropdownLabel = text;
      _isTapped = !_isTapped;
    });
  }

  Widget _buildDropdownBar(String label) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only( top: Dimens.size20, left: Dimens.size25, right: Dimens.size25),
      padding: const EdgeInsets.symmetric(vertical: Dimens.size15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Dimens.size8),
        border: Border.all(style: BorderStyle.solid, width: Dimens.width05),
        color: Theme.of(context).cardTheme.color,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: Dimens.size15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(label, style: Theme.of(context).textTheme.headline),
            if (_isTapped)
              Transform.rotate(
                  angle: pi, //When the bar is tapped make the arrow turn upward
                  child: Icon(Icons.arrow_drop_down))
            else Icon(Icons.arrow_drop_down),
          ],
        ),
      ),
    );
  }

  Widget _buildDropdownBox(List<Province> list) {
    return Container(
      height: Dimens.dropdownBox,
      margin: const EdgeInsets.symmetric(horizontal: Dimens.size25),
      child: ListView(
        padding: const EdgeInsets.all(Dimens.size0),
        children: <Widget>[
          for (int i = 0; i < list.length; i++) ...[
            _buildProvinceLabel(context ,list[i].name),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                for (var item in list[i].districts)
                  _buildStoreLocationButton(item.name),
              ],
            ),
          ],
        ],
      ),
    );
  }

  Widget _buildProvinceLabel(BuildContext context, String label) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.only(top: Dimens.size20, bottom: Dimens.size14, left: Dimens.size15),
        child: Text(label, style: Theme.of(context).textTheme.headline),
      ),
    );
  }

  Widget _buildStoreLocationButton(String label) {
    return GestureDetector(
      onTap: () {
        _storeSelected(label);
      },
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.only( top: Dimens.size25, bottom: Dimens.size25, left: Dimens.size30),
        decoration: BoxDecoration(
          border: Border( bottom: BorderSide(width: Dimens.width03)),
          color: Theme.of(context).cardTheme.color,
        ),
        child: Text(label, style: Theme.of(context).textTheme.headline),
      ),
    );
  }
}