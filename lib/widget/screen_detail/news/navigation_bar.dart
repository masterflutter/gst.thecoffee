import 'package:GST.TheCoffee/generated/coffee_icons.dart';
import 'package:GST.TheCoffee/styles/colors.dart';
import 'package:GST.TheCoffee/styles/dimens.dart';
import 'package:GST.TheCoffee/widget/base/base_widget.dart';
import 'package:flutter/material.dart';

const _paddingScreen = EdgeInsets.all(Dimens.size25);
const _size10 = Dimens.size10;
const _sizeIcon = Dimens.size44;

class NavigationBar extends BaseStatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Theme.of(context).cardTheme.color,
      elevation: 1,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(0)),
      ),
      margin: const EdgeInsets.all(0),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _flatIconTitle(
                context,
                Icon(
                  CoffeeIcon.reward,
                  color: Theme.of(context).primaryColor,
                  size: _sizeIcon,
                ),
                translator.text("save_point"),
                () => {},
            ),
            _flatIconTitle(
                context,
                Icon(
                  CoffeeIcon.deliverys,
                  color: Theme.of(context).primaryColor,
                  size: _sizeIcon,
                ),
                translator.text("order"),
                () => {},
            ),
            _flatIconTitle(
                context,
                Icon(
                  CoffeeIcon.coupons,
                  color: Theme.of(context).primaryColor,
                  size: _sizeIcon,
                ),
                translator.text("coupon"),
                () => {},
            ),
            _flatIconTitle(
                context,
                Icon(
                  CoffeeIcon.changeCoupon,
                  color: Theme.of(context).primaryColor,
                  size: _sizeIcon,
                ),
                translator.text("change_coupon"),
                () => {},
            ),
          ],
        ),
      ),
    );
  }

  Widget _flatIconTitle(
      BuildContext context, Icon icon, String title, VoidCallback onTap) {
    return InkWell(
      onTap: () => onTap(),
      child: Column(
        children: <Widget>[
          icon,
          const SizedBox(height: _size10),
          Text('$title', style: Theme.of(context).textTheme.body1)
        ],
      ),
    );
  }
}