import 'package:GST.TheCoffee/styles/dimens.dart';
import 'package:GST.TheCoffee/widget/base/base_widget.dart';
import 'package:GST.TheCoffee/widget/screen_detail/common/border_buttom.dart';
import 'package:flutter/material.dart';

const _paddingScreen = EdgeInsets.all(Dimens.size12);
const _sizeBoxHeight40 = SizedBox(height: Dimens.size40);
const _sizeBoxWidth08 = SizedBox(width: Dimens.size8);

class CardNews extends BaseStatelessWidget {

  CardNews({@required this.onTap, this.urlImage, this.title, this.content, this.titleButton});

  final String urlImage;
  final String title;
  final String content;
  final String titleButton;
  VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Card(
          color: Theme.of(context).cardTheme.color,
          elevation: 1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(Dimens.cardBorderRadius)),
          ),
          child: Container(
            width: 235,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 120,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(Dimens.cardBorderRadius),
                        topRight: Radius.circular(Dimens.cardBorderRadius),
                    ),
                    child: Image.network(
                      "$urlImage",
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width,
                    ),
                  ),
                ),
                Container(
                  padding: _paddingScreen,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '$title',
                        style: Theme.of(context).textTheme.title,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        '$content',
                        style: Theme.of(context).textTheme.body1,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                      _sizeBoxHeight40,
                      BorderButton(
                        titleButton: "$titleButton",
                        onTap: () => onTap(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        _sizeBoxWidth08,
      ],
    );
  }
}