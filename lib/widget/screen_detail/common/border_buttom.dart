import 'package:GST.TheCoffee/styles/colors.dart';
import 'package:GST.TheCoffee/styles/dimens.dart';
import 'package:flutter/material.dart';

class BorderButton extends StatefulWidget {
  BorderButton(
      {this.titleButton,
      this.onTap,
      this.defaultColor,
      this.onTapColor = CoffeeColors.buttonSelectedBottomNavigationColor});

  final String titleButton;
  final VoidCallback onTap;
  final Color defaultColor;
  final Color onTapColor;

  @override
  _BorderButtonState createState() => _BorderButtonState();
}

class _BorderButtonState extends State<BorderButton> {
  bool _isOnTap = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: Dimens.size4, horizontal: Dimens.size12),
      decoration: BoxDecoration(
        color: _isOnTap ? widget.onTapColor : Theme.of(context).cardTheme.color,
        borderRadius: BorderRadius.all(Radius.circular(Dimens.size32)),
        border: Border.all(color: Theme.of(context).primaryColor),
      ),
      child: InkWell(
        onTap: () {
          widget.onTap();
          setState(() {
            _isOnTap = !_isOnTap;
          });
        },
        child: Text(
          '${widget.titleButton ?? ""}',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.body2,
        ),
      ),
    );
  }
}
