import 'package:GST.TheCoffee/utility/app_configuration.dart';
import 'package:GST.TheCoffee/utility/global_translation.dart';
import 'package:flutter/material.dart';

import '../../dependencies.dart';

abstract class BaseStatelessWidget extends StatelessWidget {
  final AppConfiguration config = AppDependencies.injector.get<AppConfiguration>();
//  final UserPreference preference = AppDependencies.injector.get<UserPreference>();
  final GlobalTranslations translator = AppDependencies.injector.get<GlobalTranslations>();
}

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  final AppConfiguration config = AppDependencies.injector.get<AppConfiguration>();
//  final UserPreference preference = AppDependencies.injector.get<UserPreference>();
  final GlobalTranslations translator = AppDependencies.injector.get<GlobalTranslations>();
}