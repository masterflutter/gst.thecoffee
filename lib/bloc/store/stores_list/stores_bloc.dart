import 'package:GST.TheCoffee/bloc/store/stores_list/stores_event.dart';
import 'package:GST.TheCoffee/bloc/store/stores_list/stores_state.dart';
import 'package:GST.TheCoffee/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class StoresBloc extends Bloc<StoresEvent, StoresState> {
  StoresBloc({@required this.storesRepository}) : assert(storesRepository != null,"storesRepository not null");

  final StoresRepository storesRepository;

  @override
  StoresState get initialState => StoresEmpty();

  @override
  Stream<StoresState> mapEventToState(StoresEvent event) async* {
    if (event is FetchStores) {
      yield StoresLoading();
      try {
        final list = await storesRepository.getStores();
        yield StoresLoaded(storesList: list);
      } catch (_) {
        StoresError();
      }
    }
  }
}
