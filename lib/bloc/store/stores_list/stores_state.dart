import 'package:GST.TheCoffee/model/store/province_list.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class StoresState extends Equatable {
  const StoresState();

  @override
  List<Object> get props => [];
}

class StoresEmpty extends StoresState {}

class StoresLoading extends StoresState{}

class StoresLoaded extends StoresState {
  const StoresLoaded({@required this.storesList}) : assert(storesList != null,"storesList not null");

  final ProvinceList storesList;

  @override
  List<Object> get props => [storesList];
}

class StoresError extends StoresState {}
