import 'package:equatable/equatable.dart';

abstract class StoresEvent extends Equatable {
  const StoresEvent();

  @override
  List<Object> get props => [];
}

class FetchStores extends StoresEvent {}