import 'package:GST.TheCoffee/bloc/screen_detail/news/news_promotion/news_promotion_event.dart';
import 'package:GST.TheCoffee/bloc/screen_detail/news/news_promotion/news_promotion_state.dart';
import 'package:GST.TheCoffee/repositories/repositories.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';

class NewsPromotionBloc extends Bloc<NewsPromotionEvent, NewsPromotionState>{
  NewsPromotionBloc({@required this.repository}) : assert(repository != null, "");

  final CardNewPromotionRepository repository;

  @override
  NewsPromotionState get initialState => InitialNewsPromotionState();

  @override
  Stream<NewsPromotionState> mapEventToState(NewsPromotionEvent event) async* {
    if(event is LoadNewsPromotionEvent){
      try{
        final items = await repository.getAllCard();
        yield LoadedNewsPromotionState(listNewsPromotion: items);
      }catch(_){
        yield ErrorNewsPromotionState();
      }
  
    }
  }
}