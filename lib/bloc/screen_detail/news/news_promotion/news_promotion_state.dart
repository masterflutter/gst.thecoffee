import 'package:GST.TheCoffee/model/news/news_promotion.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NewsPromotionState extends Equatable{
 const NewsPromotionState();

 @override
  List<Object> get props => [];
}

class InitialNewsPromotionState extends NewsPromotionState{}

class ErrorNewsPromotionState extends NewsPromotionState{}

class NoDataNewsPromotionState extends NewsPromotionState{}

class LoadedNewsPromotionState extends NewsPromotionState{

  const LoadedNewsPromotionState({
    @required this.listNewsPromotion,
  }) : assert(listNewsPromotion != null, "listNewsPromotion khong duoc null");

  final List<NewsPromotion> listNewsPromotion;

  @override
  List<Object> get props => [listNewsPromotion];

  @override
  String toString() => 'Loaded { items: ${listNewsPromotion.length} }';
}
