import 'package:equatable/equatable.dart';

abstract class NewsPromotionEvent extends Equatable{
  const NewsPromotionEvent();

  @override
  List<Object> get props => [];
}

class LoadNewsPromotionEvent extends NewsPromotionEvent{}
