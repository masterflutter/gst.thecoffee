import 'package:GST.TheCoffee/bloc/screen_detail/news/news_promotion/news_promotion_event.dart';
import 'package:GST.TheCoffee/bloc/screen_detail/news/news_promotion/news_promotion_state.dart';
import 'package:GST.TheCoffee/repositories/api_news/card_news_repository.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';

class NewsBloc extends Bloc<NewsPromotionEvent, NewsPromotionState>{
  NewsBloc({@required this.repository}) : assert(repository != null, "");

  final CardNewRepository repository;

  @override
  NewsPromotionState get initialState => InitialNewsPromotionState();

  @override
  Stream<NewsPromotionState> mapEventToState(NewsPromotionEvent event) async* {
    if(event is LoadNewsPromotionEvent){
      try{
        final items = await repository.getAllCard();
        yield LoadedNewsPromotionState(listNewsPromotion: items);
      }catch(_){
        yield ErrorNewsPromotionState();
      }

    }
  }
}