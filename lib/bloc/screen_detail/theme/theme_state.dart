import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ThemeState extends Equatable {
  const ThemeState();

  @override
  List<Object> get props => [];
}

class InitialThemeState extends ThemeState {}

class LoadedThemeState extends ThemeState {
  const LoadedThemeState({@required this.themeData}) : assert(themeData != null,"");

  final ThemeData themeData;

  @override
  List<Object> get props => [themeData];

//  @override
//  String toString() {
//    return 'state theme data: ${themeData.toString()}';
//  }

}

class ErrorThemeState extends ThemeState {}
