import 'package:GST.TheCoffee/theme/app_themes.dart';
import 'package:GST.TheCoffee/theme/theme_light.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();

  @override
  List<Object> get props => [];
}

class ThemeChangedEvent extends ThemeEvent {
  const ThemeChangedEvent({@required this.themeData}) : assert(themeData != null, "");

  final AppTheme themeData;

  @override
  List<Object> get props => [themeData];
}
