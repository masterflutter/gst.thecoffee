import 'package:GST.TheCoffee/bloc/screen_detail/theme/theme_event.dart';
import 'package:GST.TheCoffee/bloc/screen_detail/theme/theme_state.dart';
import 'package:GST.TheCoffee/theme/app_themes.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ThemeBloc extends Bloc<ThemeChangedEvent, ThemeState>{

  @override
  ThemeState get initialState => LoadedThemeState(themeData: appThemeData[AppTheme.lightTheme]);

  @override
  Stream<ThemeState> mapEventToState(ThemeChangedEvent event) async* {
    if(event is ThemeChangedEvent){
      yield LoadedThemeState(themeData: appThemeData[event.themeData]);
    }
  }
}