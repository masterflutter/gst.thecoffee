import 'package:GST.TheCoffee/phone/account/information_screen.dart';
import 'package:flutter/material.dart';
import '../../generated/coffee_icons.dart';
import '../../styles/colors.dart';
import '../../styles/dimens.dart';
import '../../widget/base/base_widget.dart';
import '../news/news.dart';

class AccountPage extends BaseStatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _appbarAccount(context),
          _bodyAccount(context),
          ListTile(
            title: Text(translator.text("body_listmenu_button_logout")),
            onTap: (){},
          ),
        ],
      ),
    );
  }

  Widget _bodyAccount(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: Dimens.size10),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).cardTheme.color,
        border: Border.all(
          color: Theme.of(context).highlightColor,
          style: BorderStyle.solid,
          width: Dimens.border01,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            leading: Icon(CoffeeIcon.lightMode, color: Theme.of(context).iconTheme.color),
            title: Text('Dark mode', style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
         ListTile(
            leading: Icon(CoffeeIcon.reward, color: Theme.of(context).iconTheme.color),
            title: Text(translator.text("body_listmenu_title_rewards"), style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
         ListTile(
            leading: Icon(Icons.person_outline, color: Theme.of(context).iconTheme.color),
            title: Text(translator.text("body_listmenu_title_infomation"), style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.queue_music, color: Theme.of(context).iconTheme.color),
            title: Text(translator.text("body_listmenu_title_music"), style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.history, color: Theme.of(context).iconTheme.color),
            title: Text(translator.text("body_listmenu_title_history"), style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
           ListTile(
            leading: Icon(Icons.payment, color: Theme.of(context).iconTheme.color),
            title: Text(translator.text("body_listmenu_title_payment"), style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
           ListTile(
            leading: Icon(CoffeeIcon.help, color: Theme.of(context).iconTheme.color),
            title: Text(translator.text("body_listmenu_title_help"), style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.settings, color: Theme.of(context).iconTheme.color),
            title: Text(translator.text("body_listmenu_title_setting"), style: Theme.of(context).textTheme.headline),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Widget _appbarAccount(BuildContext context) {
    return Container(
      height: Dimens.appbarBox,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).cardTheme.color,
        border: Border(
          bottom: BorderSide(
              color: Theme.of(context).cardTheme.color,
              style: BorderStyle.solid,
              width: Dimens.border02,
          ),
        ),
      ),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: Dimens.size30, left: Dimens.size15),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => InformationScreen()));
              },
              child: CircleAvatar(
                  backgroundImage: AssetImage(translator.text("circle_avatar")),
                  radius: Dimens.size36,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: Dimens.size60, left: Dimens.size15),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => InformationScreen()));
              },
              child: Column(
                children: <Widget>[
                  Text(
                    ///FIXME: APPLY BLOC DATA LATER
                    'Tran Huu Long',
                    style: Theme.of(context).textTheme.headline.copyWith(fontSize: Dimens.size25),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: Dimens.size4, right: Dimens.size60),
                    child: Text(
                        translator.text("subtitle_member"),
                        style: Theme.of(context).textTheme.subhead,
                    ),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => InformationScreen()));
            },
            child: Container(
              margin: const EdgeInsets.only(left: Dimens.size100, top: Dimens.size28),
              child: Icon(CoffeeIcon.coffee, color: CoffeeColors.mediumProcess),
            ),
          ),
        ],
      ),
    );
  }
}