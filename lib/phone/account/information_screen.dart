import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import '../../generated/coffee_icons.dart';
import '../../styles/colors.dart';
import '../../styles/dimens.dart';
import '../../widget/base/base_widget.dart';

const _heightAppBar = 200.0;

class InformationScreen extends BaseStatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(_heightAppBar),
        child: _appBarWidget(context),
      ),
      body: Column(
        children: <Widget>[
          _titlePersonInfo(context),
          _detailPersonInfo(context),
        ],
      ),
    );
  }

  Widget _appBarWidget(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Theme.of(context).cardTheme.color,
      padding: const EdgeInsets.only(top: Dimens.size30),
      child: Center(
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(
                Icons.cancel,
                size: Dimens.size30,
                color: Theme.of(context).iconTheme.color,
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            CircleAvatar(
              backgroundImage: AssetImage(translator.text("circle_avatar")),
              radius: Dimens.avatarIconSize,
              child: Container(
                margin: EdgeInsets.only(top: Dimens.size72, left: Dimens.size72),
                height: Dimens.size60,
                width: Dimens.size60,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: CoffeeColors.cardColorDark,
                  border: Border.all(
                      color: CoffeeColors.cardColor,
                      width: Dimens.size3,
                      style: BorderStyle.solid,
                  ),
                ),
                child: Icon(Icons.mode_edit, size: Dimens.size15),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: Dimens.appbarBox),
              child: Row(
                children: <Widget>[
                  Icon(CoffeeIcon.coffee, color: CoffeeColors.mediumProcess),
                  Divider(
                    height: Dimens.size45,
                    endIndent: Dimens.size10,
                  ),
                  Container(
                    color: Colors.black,
                    height: Dimens.size20,
                    width: Dimens.width15,
                  ),
                  Divider(
                    height: Dimens.size45,
                    indent: Dimens.size10,
                  ),
                  ///FIXME: APPLY BLOC DATA LATAER
                  Text(translator.text("subtitle_member")),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _titlePersonInfo(BuildContext context) {
    return ListTile(
      leading: Text(
        translator.text("info_person"),
        style: Theme.of(context).textTheme.title.copyWith(fontSize: Dimens.size20),
      ),
      trailing: InkWell(
        onTap: () {},
        child: Text(
          translator.text("change"),
          style: Theme.of(context).textTheme.title.copyWith(color: CoffeeColors.softBlueColor),
        ),
      ),
    );
  }

  Widget _detailPersonInfo(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Theme.of(context).cardTheme.color,
      child: Column(
        children: <Widget>[
          _divider(context),
          ListTile(
            title: Text(translator.text("name"), style: Theme.of(context).textTheme.body1),
            subtitle: Text(
              ///FIXME: APPLY BLOC DATA LATER
              'Trần Hữu Long',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          _divider(context),
          ListTile(
            title: Text(translator.text("birthday"), style: Theme.of(context).textTheme.body1),
            subtitle: Text(
              ///FIXME: APPLY BLOC DATA LATER
              '06/05/1997',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          _divider(context),
          ListTile(
            title: Text(translator.text("phone_number"), style: Theme.of(context).textTheme.body1),
            subtitle: Text(
              ///FIXME: APPLY BLOC DATA LATER
              '0123456789',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          _divider(context),
          ListTile(
            title: Text(translator.text("email"), style: Theme.of(context).textTheme.body1),
            subtitle: Text(
              ///FIXME: APPLY BLOC DATA LATER
              'long123@fpt.com.vn',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
        ],
      ),
    );
  }
  //Horizontal line
  Widget _divider(context){
    return Divider(
      color: Theme.of(context).highlightColor,
      height: 0,
      thickness: Dimens.border02,
    );
  }
}