import 'package:GST.TheCoffee/bloc/screen_detail/theme/theme_bloc.dart';
import 'package:GST.TheCoffee/bloc/screen_detail/theme/theme_event.dart';
import 'package:GST.TheCoffee/theme/app_themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DarkMode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.color,
        elevation: Theme.of(context).appBarTheme.elevation,
        title: const  Text('Preferences'),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: AppTheme.values.length,
        itemBuilder: (context, index) {
          final itemAppTheme = AppTheme.values[index];
          return Card(
            child: ListTile(
              title: Text(
                itemAppTheme.toString(),
              ),
              onTap: () {
                BlocProvider.of<ThemeBloc>(context).add(
                  ThemeChangedEvent(themeData: itemAppTheme),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
