import 'package:GST.TheCoffee/generated/coffee_icons.dart';
import 'package:GST.TheCoffee/phone/stores/stores.dart';
import 'package:flutter/material.dart';
import '../styles/colors.dart';
import '../widget/base/base_widget.dart';
import 'account/account.dart';
import 'account/dark_mode.dart';
import 'news/news.dart';
import 'order/orders.dart';

class Phone extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends BaseState<Phone> {
  int _selectedIndex = 0;
  final List<Widget> _widgetOptions = <Widget>[
    NewsPage(),
    DarkMode(),
    StoresPage(),
    AccountPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  Widget _bottomNavigationBar() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            width: 0.3,
            color: CoffeeColors.lineNavigationBottom,
          ),
        ),
      ),
      child: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(CoffeeIcon.news),
            title: Text(translator.text("news")),
          ),
          BottomNavigationBarItem(
            icon: Icon(CoffeeIcon.delivery),
            title: Text(translator.text("order")),
          ),
          BottomNavigationBarItem(
            icon: Icon(CoffeeIcon.shop),
            title: Text(translator.text("store")),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            title: Text(translator.text("account")),
          ),
        ],
        type: BottomNavigationBarType.fixed,
        backgroundColor: Theme.of(context).cardTheme.color,
        elevation: Theme.of(context).cardTheme.elevation,
        showUnselectedLabels: true,
        unselectedIconTheme: IconThemeData(color: Theme.of(context).textTheme.title.color),
        unselectedLabelStyle: TextStyle(color: Theme.of(context).textTheme.body1.color),
        unselectedItemColor: Theme.of(context).textTheme.body1.color,
        selectedItemColor: Theme.of(context).primaryColor,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
