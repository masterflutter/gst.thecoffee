import 'package:GST.TheCoffee/bloc/store/stores_list/stores_bloc.dart';
import 'package:GST.TheCoffee/repositories/api_stores/stores_api_client.dart';
import 'package:GST.TheCoffee/repositories/repositories.dart';
import 'package:GST.TheCoffee/widget/screen_detail/store/store_appbar.dart';
import 'package:GST.TheCoffee/widget/screen_detail/store/store_dropdown_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

class StoresPage extends StatelessWidget {
  static final StoresRepository storesRepository = StoresRepository(
    storesAPIClient: StoresAPIClient(
      httpClient: http.Client(),
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Column(
        children: <Widget>[
          StoreAppbar(),
          BlocProvider(
            create: (context) => StoresBloc(storesRepository: storesRepository),
            child: StoreDropDownBar(),
          ),
        ],
      ),
    );
  }
}
