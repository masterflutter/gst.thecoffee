import 'package:GST.TheCoffee/bloc/screen_detail/news/news_promotion/bloc.dart';
import 'package:GST.TheCoffee/bloc/screen_detail/news/news_promotion/news/news_bloc.dart';
import 'package:GST.TheCoffee/generated/coffee_icons.dart';
import 'package:GST.TheCoffee/repositories/api_news/card_news_repository.dart';
import 'package:GST.TheCoffee/repositories/repositories.dart';
import 'package:GST.TheCoffee/styles/colors.dart';
import 'package:GST.TheCoffee/styles/dimens.dart';
import 'package:GST.TheCoffee/widget/base/base_widget.dart';
import 'package:GST.TheCoffee/widget/screen_detail/common/card_news.dart';
import 'package:GST.TheCoffee/widget/screen_detail/news/navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

const _heightAppBar = Dimens.heightAppBar;
const _paddingScreen = EdgeInsets.only(top: Dimens.size20, bottom: Dimens.size20, left: Dimens.size20, right: Dimens.size20);
const _radiusCircleAvatar = Dimens.size48;
const _indentVertical = Dimens.indentVertical;
const _sizeBox = Dimens.size12;
const _sizeHeight8 = SizedBox(height: Dimens.size8);
const _sizeHeight20 = SizedBox(height: Dimens.size20);

class NewsPage extends StatefulWidget{
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends BaseState<NewsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(_heightAppBar),
        child: _appBar(context),
      ),
      body: MultiBlocProvider(
        providers: [
          BlocProvider<NewsPromotionBloc>(
            create: (context) => NewsPromotionBloc(repository: CardNewPromotionRepository())..add(LoadNewsPromotionEvent()),
          ),

          BlocProvider<NewsBloc>(
            create: (context) => NewsBloc(repository: CardNewRepository())..add(LoadNewsPromotionEvent()),
          ),
        ],

        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _sizeHeight8,
              NavigationBar(),
              Container(
                padding: const EdgeInsets.only(top: Dimens.size20, left: Dimens.size20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(translator.text("what_news")),
                    _sizeHeight8,
                    _whatNews(),
                    _sizeHeight20,
                    Text(translator.text("news")),
                    _sizeHeight8,
                    _news(),
                    _sizeHeight20,
                    Text(translator.text("coffee_lover")),
                    _sizeHeight8,
                    _whatNews(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _appBar(context) {
    return Card(
      margin: const EdgeInsets.all(0),
      color: Theme.of(context).cardTheme.color,
      elevation: 1,
      shape: Border(
        bottom: BorderSide(
          width: Dimens.width01,
          color: Theme.of(context).highlightColor,
        ),
      ),
      child: Padding(
        padding: _paddingScreen,
        child: InkWell(
          onTap: () => {},
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: _radiusCircleAvatar,
                    height: _radiusCircleAvatar,
                    decoration: BoxDecoration(
                      color: CoffeeColors.cardColor,
                      shape: BoxShape.circle,
                      border: Border.all(color: CoffeeColors.lineNavigationBottom),
                    ),
                  ),
                  const SizedBox(width: _sizeBox),
                  Text(translator.text("new_customer")),
                  const  SizedBox(width: _sizeBox),
                  const VerticalDivider(
                    color: Colors.black,
                    indent: _indentVertical,
                  ),
                  const SizedBox(width: _sizeBox),
                  const Icon(CoffeeIcon.coffee, color: Colors.brown),
                ],
              ),
              IconButton(
                icon: Icon(Icons.notifications_none, color: Colors.black45),
                onPressed: () => {},
              ),
            ],
          ),
        ),
      ),
    );
  }
  
  Widget _whatNews(){
    return BlocBuilder<NewsPromotionBloc, NewsPromotionState>(
      builder: (context,state){
        if(state is LoadedNewsPromotionState){
          final cardLists = state.listNewsPromotion;
          if (state.listNewsPromotion.isEmpty) {
            return Center(
              child: Text(translator.text("empty_content")),
            );
          }
          return SizedBox(
            height: 320,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: state.listNewsPromotion.length,
              itemBuilder: (context, index){
                return CardNews(
                  title: cardLists[index].title,
                  urlImage: cardLists[index].image,
                  content: cardLists[index].content,
                  titleButton: cardLists[index].button ?? translator.text("detail"),
                  onTap: () {},
                );
              },
            ),
          );
        }
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
    );
  }

  Widget _news(){
    return BlocBuilder<NewsBloc, NewsPromotionState>(
        builder: (context,state){
          if(state is LoadedNewsPromotionState){
            final cardLists = state.listNewsPromotion;
            if (state.listNewsPromotion.isEmpty) {
              return Center(
                child: Text(translator.text("empty_content")),
              );
            }
            return SizedBox(
              height: 320,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: state.listNewsPromotion.length,
                itemBuilder: (context, index){
                  return CardNews(
                    title: cardLists[index].title,
                    urlImage: cardLists[index].image,
                    content: cardLists[index].content,
                    titleButton: cardLists[index].button ?? translator.text("detail"),
                    onTap: () {},
                  );
                },
              ),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
    );
  }
}