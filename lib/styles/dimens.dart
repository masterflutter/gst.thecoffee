///Define all dimens here
class Dimens {
  static const size0 = 0.0;
  static const size2 = 2.0;
  static const size3 = 3.0;
  static const size4 = 4.0;
  static const size5 = 5.0;
  static const size8 = 8.0;
  static const size10 = 10.0;
  static const size12 = 12.0;
  static const size14 = 14.0;
  static const size15 = 15.0;
  static const size16 = 16.0;
  static const size18 = 18.0;
  static const size20 = 20.0;
  static const size23 = 23.0;
  static const size24 = 24.0;
  static const size25 = 25.0;
  static const size28 = 28.0;
  static const size30 = 30.0;
  static const size32 = 32.0;
  static const size36 = 36.0;
  static const size40 = 40.0;
  static const size44 = 44.0;
  static const size45 = 45.0;
  static const size48 = 48.0;
  static const size60 = 60.0;
  static const size72 = 72.0;
  static const size100 = 100.0;
  static const marginBottomFloatingButton = 44.0;
  /* Height BackgroundBar */
  static const heightBackgroundBar = 102.0;

  /* Typography */
  static const typographySmall = 10.0;
  static const typographyCaption = 12.0;
  static const typographyBody = 14.0;
  static const typographySubheading = 16.0;
  static const typographyHeading = 18.0;
  static const typographyTitle = 20.0;
  static const typographyHeadline = 24.0;
  static const typographyDisplay1 = 28.0;
  static const typographyDisplay2 = 32.0;
  static const typographyDisplay3 = 36.0;
  static const typographyDisplay4 = 40.0;
  static const typographyDisplay5 = 44.0;
  static const typographyDisplay6 = 48.0;
  static const typographyDisplay7 = 52.0;
  static const typographyDisplay8 = 56.0;
  static const typographyDisplay9 = 60.0;

  /* Card */
  static const esisCardHeight = 152.0;
  static const contactCardRatio = 2.375;
  static const cardBorderRadius = 8.0;
  static const cardShadowBlur = 16.0;
  static const cardPadding = 16.0;
  static const cardElevation = 2.0;

  static const textHeight = 24.0;
  static const avatarIconSize = 48.0;

  /* Progress bar */
  static const progressBarHeight = 6.0;
  static const progressBarBorderRadius = 8.0;

  /*Navigation bar  */
  static const titleIconSize = 24.0;

  /* App bar */
  static const heightAppBar = 80.0;
  static const indentVertical = 60.0;
  static const storeAppbarHeight = 100.0;






  
  /*Step*/
  static const paddingRightStep = 5.0;
  static const widthTextStep = 63.0;
  static const heightTextStep = 25.0;
  static const heightListStep = 70.0;

  /*Border thickness*/
  static const width01 = 0.1;
  static const width03 = 0.3;
  static const width05 = 0.5;
  static const width15 = 1.5;
  /*Box*/
  static const dropdownBox = 400.0; 
  static const menuBox = 400.0;
  static const appbarBox = 130.0;

  /*  Title */
  static const titleName = 24.0;
  /*Subtitle */
  static const subtitle = 15.0;

  /* Border Width */
  static const border01 = 0.1;
  static const border02 = 0.2;
  static const border03 = 0.3;
  static const border04 = 0.4;
  static const border05 = 0.5;
  static const border15 = 1.5;
}
