import 'package:flutter/material.dart';

///Define all colors here
class CoffeeColors {
  //Common color
  static const iconActiveColor = Color.fromRGBO(103, 145, 244, 1);
  static const iconDisableColor = Color.fromRGBO(195, 206, 231, 1);
  static const topNavigationBarColor = Color.fromRGBO(31, 85, 169, 1);
  static const botNavigationBarColor = Color.fromRGBO(36, 42, 111, 1);
  static const softBlueColor = Color.fromRGBO(103, 145, 244, 1);
  static const veryLightBlueColor = Color.fromRGBO(228, 235, 251, 1);
  static const blueGreyColor = Color.fromRGBO(125, 132, 151, 1);
  static const charcoalGrey = Color.fromRGBO(51, 43, 50, 1);
  static const greyColor = Color.fromRGBO(243, 247, 252, 1);
  static const greyLabel = Color.fromRGBO(242, 242, 242, 1);
  static const battleShipGrey = Color.fromRGBO(110, 118, 136, 1);
  static const paleGreyTwo = Color.fromRGBO(244, 247, 251, 1);
 
  //Esis cell
  static const cardShadowColor = Color.fromRGBO(0, 0, 0, 0.08);
  static const cardColor = Color.fromRGBO(254, 255, 254, 1);
  static const cardColorDark = Color.fromRGBO(30, 30, 30, 1);
  static const esisCardEmptyCustomerTextColor = Color.fromRGBO(134, 167, 243, 1);

  static const borderColorButton = Color.fromRGBO(254, 149, 0, 1);

  static const buttonSelectedBottomNavigationColor = Color.fromRGBO(234, 128, 37, 1);


  /* Background */
  static const backgroundColor = Color.fromRGBO(239, 239, 244, 1);
  static const backgroundColorDark = Color.fromRGBO(18, 18, 18, 1);
  static const primaryColor = Color.fromRGBO(234, 128, 37, 1);
  static const primaryColorDark = Color.fromRGBO(234, 128, 37, 0.7);
//  static const primaryColorDark = Colors.orange[200];
  static const accentColor = Color.fromRGBO(103, 145, 244, 1);

  /*App bar theme*/
  static const backgroundAppBarColorDark = Color.fromRGBO(32, 32, 32, 1);
  static const backgroundAppBarColor = Color.fromRGBO(255, 255, 255, 1);


  //FloatActionButton
  static const childLabelFloatButtonColor = Color.fromRGBO(255, 255, 255, 1);
  static const backgroundFloatButtonColor = Color.fromRGBO(8, 14, 27, 0.7);

  /* ProcessIndicator */
  static const lowProcess = Color.fromRGBO(237, 84, 88, 1);
  static const mediumProcess = Color.fromRGBO(239, 185, 90, 1);
  static const highProcess = Color.fromRGBO(13, 166, 111, 1);
  static const progressColor = Color.fromRGBO(240, 244, 248, 1);

  /* Border Navigation bottom */
  static const lineNavigationBottom = Color.fromRGBO(127, 127, 127, 1);


  /*  Text Style  */
    static const textColor = Colors.black;
}
