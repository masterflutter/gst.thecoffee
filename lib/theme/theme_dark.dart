import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../styles/colors.dart';
import '../styles/dimens.dart';

class CoffeeThemeDark {
  static ThemeData buildTheme() {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
      ),
    );

    return ThemeData(
      //NOT MODIFY
      appBarTheme: AppBarTheme(color: CoffeeColors.backgroundAppBarColorDark, elevation: 1),
      //NOT MODIFY
      backgroundColor: CoffeeColors.backgroundColorDark,
      //NOT MODIFY
      bottomAppBarTheme: BottomAppBarTheme(
        color: CoffeeColors.backgroundAppBarColorDark,
        elevation: 2,
      ),
      accentColor: CoffeeColors.accentColor,
      //NOT MODIFY
      primaryColor: CoffeeColors.primaryColorDark,
      //NOT MODIFY
      cardTheme: CardTheme(color: CoffeeColors.cardColorDark, elevation: Dimens.cardElevation),
      //NOT MODIFY
      textSelectionColor: const  Color.fromARGB(255, 105, 113, 141),
      //NOT MODIFY
      selectedRowColor: CoffeeColors.buttonSelectedBottomNavigationColor,
      errorColor: Colors.red[600],
      //NOT MODIFY
      buttonColor: const Color.fromARGB(255, 0, 230, 118),
      //NOT MODIFY
      highlightColor: Colors.white,
      textTheme: TextTheme(
        //Title card, content, KHÔNG ĐƯỢC SỬA
          title: const TextStyle(
            color: Color.fromRGBO(255, 255, 255, 0.8),
            fontWeight: FontWeight.w600,
            fontSize: 16,
          ),
          subhead: const TextStyle(
            color: Color.fromARGB(255, 105, 113, 141),
            fontWeight: FontWeight.normal,
            fontSize: 15,
          ),
          //KHÔNG ĐƯỢC SỬA
          headline: const TextStyle(
            color: Color.fromRGBO(255, 255, 255, 0.8),
            fontWeight: FontWeight.normal,
            fontSize: 16,
          ),
          display2: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 43,
          ),
          //Default text, KHÔNG ĐƯỢC SỬA
          body1: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color.fromRGBO(110, 118, 136, 0.8),
          ),
          //NOT MODIFY
          body2: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: CoffeeColors.primaryColorDark,
          ),
          //Section title text
          overline: const TextStyle(
            color: Color.fromRGBO(110, 118, 136, 1),
            fontSize: 12,
          ),
          caption: TextStyle(
            color: Colors.black87,
            fontSize: 12,
          )
      ),
      //Icon
      iconTheme: const IconThemeData(
        color: Color.fromRGBO(255, 255, 255, 0.7),
      ),
      //Card color
      cardColor: CoffeeColors.cardColorDark,
    );
  }
}
