import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../styles/colors.dart';
import '../styles/dimens.dart';

class CoffeeTheme {
  static ThemeData buildTheme() {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
      ),
    );

    return ThemeData(
      bottomAppBarTheme: BottomAppBarTheme(
        color: CoffeeColors.cardColor,
        elevation: 2,
      ),
      //NOT MODIFY
      appBarTheme: AppBarTheme(color: CoffeeColors.backgroundAppBarColor, elevation: 1),
      //NOT MODIFY
      backgroundColor: CoffeeColors.backgroundColor,
      accentColor: CoffeeColors.accentColor,
      //NOT MODIFY
      primaryColor: CoffeeColors.primaryColor,
      //NOT MODIFY
      cardTheme: CardTheme(color: CoffeeColors.cardColor, elevation: Dimens.cardElevation),
      //NOT MODIFY
      textSelectionColor: const Color.fromARGB(255, 105, 113, 141),
      //NOT MODIFY
      selectedRowColor: CoffeeColors.buttonSelectedBottomNavigationColor,
      errorColor: Colors.red[600],
      buttonColor: const Color.fromARGB(255, 0, 230, 118),
      //NOT MODIFY
      highlightColor: Colors.black87,
      textTheme: TextTheme(
        //Title card, title content, KHÔNG ĐƯỢC SỬA
        title: const TextStyle(
          color: Color.fromRGBO(0, 0, 0, 1),
          fontWeight: FontWeight.w600,
          fontSize: 16,
        ),
        subhead: const TextStyle(
          color: Color.fromARGB(255, 105, 113, 141),
          fontWeight: FontWeight.normal,
          fontSize: 15,
        ),
        headline: const TextStyle(
          color: Color.fromRGBO(0, 0, 0, 1),
          fontWeight: FontWeight.normal,
          fontSize: 16,
        ),
        display2: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 43,
        ),
        //Default text, content, KHÔNG ĐƯỢC SỬA
        body1: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.normal,
          color: Color.fromRGBO(110, 118, 136, 1),
        ),
          //KHONG ĐƯỢC SỬA
          body2: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: CoffeeColors.primaryColor,
          ),
        //Section title text
        overline: const TextStyle(
          color: Color.fromRGBO(110, 118, 136, 1),
          fontSize: 12,
        ),
        caption: TextStyle(
          color: Colors.black87,
          fontSize: 12,
        )
      ),
      //Icon
      iconTheme: const IconThemeData(
        color: Color.fromRGBO(0, 0, 0, 0.7),
      ),
      //Card color 
      cardColor: const Color.fromRGBO(255, 255, 255, 1),
    );
  }
}
