import 'package:GST.TheCoffee/theme/theme_dark.dart';
import 'package:GST.TheCoffee/theme/theme_light.dart';
import 'package:flutter/src/material/theme_data.dart';

enum AppTheme{
  lightTheme,
  darkTheme,
}

final Map<AppTheme, ThemeData> appThemeData = {
  AppTheme.lightTheme : CoffeeTheme.buildTheme(),
  AppTheme.darkTheme : CoffeeThemeDark.buildTheme(),
};