import 'package:flutter/widgets.dart';

class CoffeeIcon {
  CoffeeIcon._();

  static const _kFontFam = 'Coffee_Icon';

  static const IconData reward = IconData(0xe800, fontFamily: _kFontFam);
  static const IconData delivery = IconData(0xe801, fontFamily: _kFontFam);
  static const IconData shop = IconData(0xe802, fontFamily: _kFontFam);
  static const IconData news = IconData(0xe803, fontFamily: _kFontFam);
  static const IconData help = IconData(0xe804, fontFamily: _kFontFam);
  static const IconData coffee = IconData(0xe805, fontFamily: _kFontFam);
  static const IconData deliverys = IconData(0xe811, fontFamily: _kFontFam);
  static const IconData coupons = IconData(0xe812, fontFamily: _kFontFam);
  static const IconData changeCoupon = IconData(0xe813, fontFamily: _kFontFam);
  static const IconData lightMode = IconData(0xe809, fontFamily: _kFontFam);
  static const IconData darkMode = IconData(0xe80a, fontFamily: _kFontFam);
}
